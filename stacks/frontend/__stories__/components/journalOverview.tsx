import { storiesOf } from '@storybook/react';
import * as React from 'react';
import { StaticRouter } from 'react-router-dom';

import { Periodical } from '../../../../lib/interfaces/Periodical';
import { ScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { JournalOverview } from '../../src/components/journalOverview/component';

storiesOf('JournalOverview', module)
  .add('All', () => {
    const periodical: Periodical = {
      creator: { identifier: 'arbitrary-id', name: 'John Doe' },
      datePublished: '2002-02-14T13:37:00.000Z',
      description: 'Studies meant to create a carbon-free living environment.',
      headline: 'Heat your home, not the planet',
      identifier: 'fifth',
      // tslint:disable-next-line:max-line-length
      image: 'https://i.imgur.com/tutO4xT.jpg',
      name: 'Zero Carbon Housing',
    };
    const articles: Array<Partial<ScholarlyArticle>> = [
      {
        // tslint:disable-next-line:max-line-length
        description: 'In this article we discuss different approaches to calculating the optimal routes for intergalactic bypasses.',
        identifier: 'arbitrary-id',
        name: 'Route optimisations for intergalactic bypasses',
      },
      {
        // tslint:disable-next-line:max-line-length
        description: 'We discuss a novel approach to relocating dwellers of homes located in the path of future bypasses.',
        identifier: 'arbitrary-id',
        name: 'On the relocation of human debris',
      },    ];

    return (
      <StaticRouter>
        <JournalOverview
          url="arbitrary-url"
          periodical={periodical}
          articles={articles}
        />
      </StaticRouter>
    );
  });
