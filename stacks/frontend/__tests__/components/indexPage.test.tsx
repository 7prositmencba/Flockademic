import { IndexPage } from '../../src/components/indexPage/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should match the snapshot', () => {
  const quip = shallow(<IndexPage/>);

  expect(toJson(quip)).toMatchSnapshot();
});
