import {
  convertToOrcid,
  convertToOrcidLink,
  convertToOrcidWorkId,
  convertToOrcidWorkLink,
  isOrcidWorkId,
  isOrcidWorkLink,
} from '../../utils/orcid';

describe('convertToOrcidWorkId', () => {
  it('should properly convert an ORCID work link to the ID we use internally', () => {
    expect(convertToOrcidWorkId('https://orcid.org/0000-0002-4013-9889/work/1337'))
      .toBe('0000-0002-4013-9889:1337');
  });

  it('should also properly convert an ORCID work link if the ORCID ID ends with an X', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(convertToOrcidWorkId('https://orcid.org/0000-0002-4013-988X/work/1337'))
      .toBe('0000-0002-4013-988X:1337');
  });
});

describe('convertToOrcidWorkLink', () => {
  it('should properly convert the IDs we use internally for ORCID works to an ORCID work link', () => {
    expect(convertToOrcidWorkLink('0000-0002-4013-9889:1337'))
      .toBe('https://orcid.org/0000-0002-4013-9889/work/1337');
  });

  it('should also properly convert the IDs we use internally for ORCID works if the ORCID ID ends with an X', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(convertToOrcidWorkLink('0000-0002-4013-988X:1337'))
      .toBe('https://orcid.org/0000-0002-4013-988X/work/1337');
  });
});

describe('isOrcidWorkId', () => {
  it('should recognise properly formatted ORCID work IDs', () => {
    expect(isOrcidWorkId('0000-0002-4013-9889:1337'))
      .toBe(true);
  });

  it('should also recognise a properly formatted ORCID work ID for an ORCID ID that ends with an X', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(isOrcidWorkId('0000-0002-4013-988X:1337'))
      .toBe(true);
  });

  it('should recognise when a string is not an ORCID work ID', () => {
    expect(isOrcidWorkId('not-0000-0002-4013-9889:1337'))
      .toBe(false);
  });
});

describe('isOrcidWorkLink', () => {
  it('should recognise properly formatted ORCID work links', () => {
    expect(isOrcidWorkLink('https://orcid.org/0000-0002-4013-9889/work/1337'))
      .toBe(true);
  });

  it('should also recognise properly formatted ORCID work links for an ORCID ID that ends with an X', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(isOrcidWorkLink('https://orcid.org/0000-0002-4013-988X/work/1337'))
      .toBe(true);
  });

  it('should recognise when a string is not an ORCID work link as we use them, e.g. without HTTPS', () => {
    expect(isOrcidWorkId('http://orcid.org/0000-0002-4013-9889/work/1337'))
      .toBe(false);
  });
});

describe('convertToOrcid', () => {
  it('should properly convert an ORCID link to the ORCID', () => {
    expect(convertToOrcid('https://orcid.org/0000-0002-4013-9889/'))
      .toBe('0000-0002-4013-9889');
  });

  it('should also properly convert an ORCID link to the ORCID when that ORCID ends with an X', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(convertToOrcid('https://orcid.org/0000-0002-4013-988X/'))
      .toBe('0000-0002-4013-988X');
  });
});

describe('convertToOrcidLink', () => {
  it('should properly convert ORCIDs to an ORCID link', () => {
    expect(convertToOrcidLink('0000-0002-4013-9889'))
      .toBe('https://orcid.org/0000-0002-4013-9889');
  });

  it('should also properly convert ORCIDs to an ORCID link when that ORCID ends with an X', () => {
    // See https://support.orcid.org/knowledgebase/articles/116780-structure-of-the-orcid-identifier
    expect(convertToOrcidLink('0000-0002-4013-988X'))
      .toBe('https://orcid.org/0000-0002-4013-988X');
  });
});
